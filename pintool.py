#!/usr/bin/env python
# -*- coding: utf8 -*-
#
# Copyright (c) 2015 Eduardo Garcia Melia <wagiro@gmail.com>
# Copyright (c) 2019 Philippe Grégoire <git@pgregoire.xyz>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import getopt
import re
import string
import subprocess
import sys


# FIXME local arguments
PIN = None
INSCOUNT = None
FILENAME = None


def get_charset(num):
    charsets = {
        0: '',
        1: string.ascii_lowercase,
        2: string.ascii_uppercase,
        3: string.digits,
        4: string.hexdigits,
        5: string.punctuation,
        6: string.printable,
    }


    r = ''
    for i in num.split(','):
        if not i:
            continue

        i = int(i)
        if 0 <= i <= len(charsets):
            r += charsets[i]
        else:
            raise Exception('Invalid charset: {}'.format(i))

    return r


def pin(passwd):
    try:
        command = "echo " + passwd + " | " + PIN + " -t " + INSCOUNT + " -- "+ FILENAME + " ; cat inscount.out"
        output = subprocess.check_output(command,shell=True,stderr=subprocess.PIPE)
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    return int(re.findall(r"Count ([\w.-]+)", output)[0])


def lengthdetect(passlen):
    d = 0

    for i in range(passlen):
        i += 1
        password = '_' * i
        inscount = pin(password)

        if 0 == d:
            d = inscount

        print("%s = with %d characters difference %d instructions" %(password, i, inscount-d))


def solve(initpass, passlen, symbfill, charset, expression, reverse):
    def addch(p, c):
        if reverse:
            return (c + p)
        return (p + c)

    print(initpass, passlen, symbfill, charset, expression, reverse)

    initlen = len(initpass)

    print('==> calculating initial instruction count')
    inscount = pin(initpass + (symbfill * (passlen - initlen)))

    print('==> starting attack')
    for i in range(initlen, passlen):
        filler = (symbfill * (passlen - i - 1))

        ends = [initpass, filler]
        if reverse:
            ends = ends[::-1]

        lastcnt = inscount

        for char in charset:
            password = ends[0] + '\\'+char + ends[1]
            assert(len(password) == (passlen + 1))
            inscount = pin(password)
            # FIXME breaks if '\' is already in the password
            newpass = password.replace("\\","", 1)
            difference = inscount - lastcnt

            print("%s = %d difference %d instructions" %(newpass, inscount, difference))
            #sys.stdout.write("\033[F")
            number = expression.split(' ')[-1]
            if "!=" in expression:
                if difference != int(number):
                    initpass = addch(initpass, char)
                    break
            elif "==" in expression:
                if difference == int(number):
                    initpass = addch(initpass, char)
                    break
            elif "<=" in expression:
                if difference <= int(number):
                    initpass = addch(initpass, char)
                    break
            elif ">=" in expression:
                if difference >= int(number):
                    initpass = addch(initpass, char)
                    break
            elif expression:
                print("Unknown value for -d option: {}".format(expression))
                sys.exit()

            if char == charset[-1]:
                print("\n\nPassword not found, try to change charset...\n")
                sys.exit()

    return password.replace("\\","",1)


def main(args):
    global INSCOUNT, FILENAME, PIN

    def usage(file=sys.stdout):
        u  = '{} [-ehr] [-a arch] [-b extrach] '.format(sys.argv[0])
        u += '[-c charset[,charset2]] [-d expr] [-i initpass] [-l len] '
        u += '[-s fillch] pinroot program'
        file.write('usage: {}\n'.format(u))

    try:
        opts, args = getopt.getopt(args, 'a:b:c:d:ehi:l:rs:')
    except getopt.GetoptError as e:
        sys.stderr.write(e + '\n')
        usage(file=sys.stderr)
        return 2


    o_arch = '32'
    o_extrach = ''
    o_charset = '1'
    o_expr    = '!= 0'
    o_init    = ''
    o_len     = 10
    o_filler  = '_'
    o_rev     = False
    o_study   = False

    for k, v in opts:
        if '-a' == k:
            o_arch = v
        elif '-b' == k:
            o_extrach = v
        elif '-c' == k:
            o_charset = v
        elif '-d' == k:
            o_expr = v
        elif '-e' == k:
            o_study = True
        elif '-h' == k:
            usage()
            return 0
        elif '-i' == k:
            o_init = v
        elif '-l' == k:
            o_len = int(v)
        elif '-r' == k:
            o_rev = True
        elif '-s' == k:
            o_filler = v

    o_charset = get_charset(o_charset) + o_extrach


    # FIXME == means the password is known, exit early
    if len(o_init) >= o_len:
        sys.stderr.write('The length of the initial password must be less than password length\n')
        return 2

    if len(o_filler) != 1:
        sys.stderr.write('Only one filling character is possible\n')
        return 2


    if 2 != len(args):
        usage(file=sys.stderr)
        return 2


    PIN = '{}/pin'.format(args[0])
    INSCOUNT = '{}/source/tools/ManualExamples'.format(args[0])
    if '32' == o_arch:
        INSCOUNT += '/obj-ia32'
    elif '64' == o_arch:
        INSCOUNT += '/obj-intel64'
    else:
        sys.stderr.write('Unknown architecture\n')
        return 2
    INSCOUNT += '/inscount0.so'


    FILENAME = args[1]
    if o_study is True:
        lengthdetect(o_len)
        return 0


    password = solve(o_init, o_len, o_filler, o_charset, o_expr, o_rev)
    print('Password: {}'.format(password))
    return 0


if '__main__' == __name__:
    sys.exit(main(sys.argv[1:]))
